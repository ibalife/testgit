package com.example.iba_dev.aidl;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int a=0;
    private int b=1;

    private String bug="修复bug";
    private String eChat="添加客服功能";
    private ValueAnimator animator=ValueAnimator.ofFloat(0,1f);
    TimeTextView timeTextView;
    private String addPage="添加一个页面";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timeTextView= (TimeTextView) findViewById(R.id.timeTextView);
        findViewById(R.id.hello).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeTextView.setTimesOnly(1000000);
                timeTextView.start();
                timeTextView.setOnTimeFinishedListener(new TimeTextView.OnTimeFinishedListener() {
                    @Override
                    public void onTimeFinished() {
                        Toast.makeText(MainActivity.this,"时间到了",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        animator.setDuration(1000);
        animator.start();
    }
}
