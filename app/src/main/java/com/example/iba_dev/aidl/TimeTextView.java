package com.example.iba_dev.aidl;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by IBA-dev on 2017/7/10.
 */
public class TimeTextView extends RelativeLayout implements Runnable {
    public static final int STYLE_IMAGE=1;
    public static final int STYLE_TEXT=2;
    private Handler handler=new Handler();
    private LinearLayout llNormalText;
    private TextView textViewAllText;
    private TextView textViewHour;
    private TextView textViewMinute;
    private TextView textViewSecond;
    private ImageView imageViewSplit;
    private ImageView imageViewSplit1;
    private int drawableTextBack;
    private int drawableSplit;
    private int styleSheet = STYLE_IMAGE;
    private long times;
    private int whatMsg;//1为即将开始，2为即将结束
    private OnTimeFinishedListener onTimeFinished;
    private int textViewColor= Color.BLACK;

    public TimeTextView(Context context) {
        super(context);
        init(context,null,0);
    }

    public TimeTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs,0);
    }

    public TimeTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs,defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        inflate(context,R.layout.time_text_view,this);
        llNormalText= (LinearLayout) findViewById(R.id.timeTextView_normalText);
        textViewAllText = (TextView) findViewById(R.id.timeTextView_allText);
        textViewHour = (TextView) findViewById(R.id.timeTextView_hour);
        textViewMinute = (TextView) findViewById(R.id.timeTextView_minute);
        textViewSecond = (TextView) findViewById(R.id.timeTextView_second);
        imageViewSplit = (ImageView) findViewById(R.id.timeTextView_split);
        imageViewSplit1 = (ImageView) findViewById(R.id.timeTextView_split1);
        if(attrs!=null){
            TypedArray typedArray=context.obtainStyledAttributes(attrs, R.styleable.TimeTextView);
            drawableTextBack=typedArray.getResourceId(R.styleable.TimeTextView_textBackground,0);
            drawableSplit=typedArray.getResourceId(R.styleable.TimeTextView_splitResource,0);
            styleSheet =typedArray.getInt(R.styleable.TimeTextView_stylesheet,0);
            textViewColor=typedArray.getColor(R.styleable.TimeTextView_textViewColor,Color.BLACK);
            typedArray.recycle();
        }
        setTextViewBackground();
        setImageViewResource();
        setTextViewColor(textViewColor);
        setViewValue();
    }


    private void setViewValue(){
        String formatLongToTimeStr;
        if(styleSheet == STYLE_TEXT) {
            formatLongToTimeStr = formatLongToTimeStr(this.times, 1);
        }else {
            formatLongToTimeStr = formatLongToTimeStr(this.times, 0);
        }
        String[] split = formatLongToTimeStr.split(":");
        if(styleSheet == STYLE_IMAGE){
            llNormalText.setVisibility(VISIBLE);
            textViewAllText.setVisibility(GONE);
            textViewHour.setText(split[0]);
            textViewMinute.setText(split[1]);
            textViewSecond.setText(split[2]);
        }else{
            llNormalText.setVisibility(GONE);
            textViewAllText.setVisibility(VISIBLE);
            String str;
            if(Integer.parseInt(split[0]) > 0){
                str = split[0]+"天 "+ split[1] + "时" + split[2] + "分" + split[3]+ "秒";
            }else {
                str = split[1] + "时" + split[2] + "分" + split[3]+ "秒";
            }
            textViewAllText.setText(str);
        }
    }

    public void setTimes(long times, int whatMsg) {
        this.times = times;
        this.whatMsg = whatMsg;
        setViewValue();
    }

    public void setTimesOnly(long times){
        this.times = times;
        setViewValue();
    }

    public void start(){
        handler.post(this);
    }

    public void stop(){
        handler.removeCallbacks(this);
    }

    private void setImageViewResource() {
        try {
            imageViewSplit.setImageResource(drawableSplit);
            imageViewSplit1.setImageResource(drawableSplit);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setTextViewBackground(){
        try {
            textViewHour.setBackgroundResource(drawableTextBack);
            textViewMinute.setBackgroundResource(drawableTextBack);
            textViewSecond.setBackgroundResource(drawableTextBack);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setTextViewBackground(int resId){
        drawableTextBack=resId;
        setTextViewBackground();
    }

    public void setTextViewBackgroundColor(int color){
        textViewHour.setBackgroundColor(color);
        textViewMinute.setBackgroundColor(color);
        textViewSecond.setBackgroundColor(color);
    }

    public void setImageViewResource(int resId){
        drawableSplit=resId;
        setImageViewResource();
    }

    public void setTextViewColor(int color){
        this.textViewColor=color;
        textViewHour.setTextColor(color);
        textViewMinute.setTextColor(color);
        textViewSecond.setTextColor(color);
        textViewAllText.setTextColor(color);
    }

    @Override
    public void run() {
        times--;
        if(times<=0){
            setTimesOnly(0);
            if(this.onTimeFinished!=null){
                onTimeFinished.onTimeFinished();
            }
            return;
        }
        setViewValue();
        handler.postDelayed(this,1000);
    }

    public void setOnTimeFinishedListener(OnTimeFinishedListener onTimeFinished){
        this.onTimeFinished=onTimeFinished;
    }

    public interface OnTimeFinishedListener{
        public void onTimeFinished();
    }

    public int getStyleSheet() {
        return styleSheet;
    }

    public void setStyleSheet(int styleSheet) {
        this.styleSheet = styleSheet;
    }

    public static String formatLongToTimeStr(Long l, int type) {//默认为零时，得到时分秒，为1时得到天时分秒
        int day = 0;
        int hour = 00;
        int minute = 00;
        int second = 00;
        String strtime;
        second = l.intValue();
        if (second > 60) {
            minute = second / 60;         //取整
            second = second % 60;         //取余
        }

        if (minute > 60) {
            hour = minute / 60;
            minute = minute % 60;
        }
        if(type == 1){
            if(hour > 24){
                day = hour / 24;
                hour = hour % 24;
            }
            strtime = String.format("%d", day)  + ":" + String.format("%02d", hour)  + ":" + String.format("%02d", minute) + ":" + String.format("%02d", second);
        }else {
            strtime = String.format("%02d", hour) + ":" + String.format("%02d", minute) + ":" + String.format("%02d", second);
        }
        return strtime;
    }

}
